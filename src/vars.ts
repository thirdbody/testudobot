export const BOTNAME: string = String(process.env.DISCORD_BOTNAME); // i.e. "Testudo"
export const SERVERNAME: string = String(process.env.DISCORD_SERVERNAME); // i.e. "University of Maryland Class of 2024";
