import * as Discord from "discord.js";
import { mention } from "./helpers";

const client: Discord.Client = new Discord.Client();

client.on("guildMemberAdd", guildMember => {
  const welcome: Discord.TextChannel = guildMember.guild.channels.find(
    channel => channel.name === "welcome"
  ) as Discord.TextChannel;
  welcome
    .send(
      `Welcome ${mention(
        guildMember.user
      )} to the UMD 2024 Discord! Please state your major to enter the server.`
    )
    .catch(console.error);
});

client.on("message", (message: Discord.Message) => {
  if (message.author.id === client.user.id) {
    return;
  }

  if (message.channel instanceof Discord.TextChannel) {
    if (message.channel.name === "welcome") {
      const foundRole: Discord.Role = message.guild.roles.find(
        role => role.name === message.content
      );
      if (foundRole) {
        message.member
          .addRole(foundRole)
          .then((member: Discord.GuildMember) => {
            message.channel.send(
              `${mention(member.user)} your role has been set! Welcome!`
            );
          })
          .catch(console.error);

        message.member
          .addRole(message.guild.roles.find(role => role.name === "Verified"))
          .catch(console.error);
      } else {
        // message.channel.send(
        //   `${mention(
        //     message.author
        //   )} that major isn't in our system yet. We've asked a moderator to approve you.`
        // );

        // modMail(
        //   message.guild,
        //   `User ${message.author.username} attempting to select major ${message.content}. Please approve/decline.`
        // );

        message.guild.createRole({ name: message.content }).then(role => {
          message.member
            .addRole(role)
            .then((member: Discord.GuildMember) => {
              message.channel.send(
                `${mention(member.user)} your role has been set! Welcome!`
              );
            })
            .catch(console.error);

          message.member
            .addRole(message.guild.roles.find(role => role.name === "Verified"))
            .catch(console.error);
        });
      }
    }
  }
});

client.login(process.env.DISCORD_TOKEN);
