import * as Discord from "discord.js";

export function mention(user: Discord.User): string {
    return "<@" + user.id + ">";
}

export function modMail(guild: Discord.Guild, message: string): void {
    const channel = guild.channels.find(channel => channel.name === "modmail") as Discord.TextChannel;
    channel.send(message).catch(console.error);
}
